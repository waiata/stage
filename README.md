# Stage

Create a naked window for output in MacOS display apps.

Extensions
- Convenient NSScreen extension

Elements
- Placement (frame constructor)
- Dimension (distance measure with pixel, portion or percent unit)
- Alignment (Horizontal and vertical alignment)
- Background (Colour and Picture to be painted on layers)
- Picture (Image file with fit)
- Colour (Codable color enum)

UI
- Stage Storyboard
- StageController (View Controller for stage settings)
- StageChooser (Control to select screen for a stage)
- ScreenPopUp (Select a sceen)
- EdgeChooser (Nine Alignment buttons)
- NumberField (Text Field with number formatting, including basic maths and bump control)
- DimensionField (Extension of NumberField with Dimension units)
- ColourWell (pick colour by clicking to bring up ColorPanel or Drag and Drop or from memory bank of recents)
- PictureWell (click to browse or drag and drop image files. Contextual menu with Alignment options and clear)
