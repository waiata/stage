import Cocoa

public enum Colour: Codable, Equatable {
    
    public typealias Part = CGFloat
    
    //MARK: Cases
    
    case unknown
    case clear
    case black(alpha: Part)
    case white(alpha: Part)
    case gray(gray: Part, alpha: Part)
    case rgb(red: Part, green: Part, blue: Part)
    case RGB(red: Part, green: Part, blue: Part)
    case rgba(red: Part, green: Part, blue: Part, alpha: Part)
    case RGBa(red: Part, green: Part, blue: Part, alpha: Part)
    case cmyk(cyan: Part, magenta: Part, yellow: Part, black: Part)
    case cmyka(cyan: Part, magenta: Part, yellow: Part, black: Part, alpha: Part)
    case CMYK(cyan: Part, magenta: Part, yellow: Part, black: Part)
    case CMYKA(cyan: Part, magenta: Part, yellow: Part, black: Part, alpha: Part)
    case hsb(hue: Part, saturation: Part, brightness: Part)
    case hsba(hue: Part, saturation: Part, brightness: Part, alpha: Part)
    case HSB(hue: Part, saturation: Part, brightness: Part)
    case HSBa(hue: Part, saturation: Part, brightness: Part, alpha: Part)
    
    init() {
        self = .clear
    }
    
    //MARK: NSColor
    
    init(_ ns: NSColor) {
        let mode = ns.type == .componentBased ? ns.colorSpace.colorSpaceModel : .unknown
        
        switch mode {
            case .gray:
                self = .gray(gray: ns.whiteComponent, alpha: ns.alphaComponent)
            case .rgb:
                if ns.alphaComponent == 1 {
                    self = .rgb(red: ns.redComponent, green: ns.greenComponent, blue: ns.blueComponent)
                } else {
                    self = .rgba(red: ns.redComponent, green: ns.greenComponent, blue: ns.blueComponent, alpha: ns.alphaComponent)
                }
        case .cmyk :
            if ns.alphaComponent == 1 {
                self = .cmyk(cyan: ns.cyanComponent, magenta: ns.magentaComponent, yellow: ns.yellowComponent, black: ns.blackComponent)
            } else {
                self = .cmyka(cyan: ns.cyanComponent, magenta: ns.magentaComponent, yellow: ns.yellowComponent, black: ns.blackComponent, alpha: ns.alphaComponent)
            }
        default:
            if let rgb = ns.usingColorSpace(.deviceRGB) {
                self = .init(rgb)
            } else {
                self = .unknown
            }
        }
    }
    
    
    var nsColor: NSColor {
        switch self {
        case .unknown: return NSColor()
        case .clear: return .clear
        case .black(alpha: let alpha):
            return NSColor(white: 0, alpha: alpha)
        case .white(alpha: let alpha):
            return NSColor(white: 1, alpha: alpha)
        case .gray(gray: let gray, alpha: let alpha):
            return NSColor(white: gray, alpha: alpha)
        case .rgb(red: let red, green: let green, blue: let blue):
            return NSColor(red: red, green: green, blue: blue, alpha: 1)
        case .RGB(red: let red, green: let green, blue: let blue):
            return NSColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
        case .rgba(red: let red, green: let green, blue: let blue, alpha: let alpha):
            return NSColor(red: red, green: green, blue: blue, alpha: alpha)
        case .RGBa(red: let red, green: let green, blue: let blue, alpha: let alpha):
            return NSColor(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
        case .cmyk(cyan: let cyan, magenta: let magenta, yellow: let yellow, black: let black):
            return NSColor(deviceCyan: cyan, magenta: magenta, yellow: yellow, black: black, alpha: 1)
        case .cmyka(cyan: let cyan, magenta: let magenta, yellow: let yellow, black: let black, alpha: let alpha):
            return NSColor(deviceCyan: cyan, magenta: magenta, yellow: yellow, black: black, alpha: alpha)
        case .CMYK(cyan: let cyan, magenta: let magenta, yellow: let yellow, black: let black):
            return NSColor(deviceCyan: cyan/100, magenta: magenta/100, yellow: yellow/100, black: black/100, alpha: 1)
        case .CMYKA(cyan: let cyan, magenta: let magenta, yellow: let yellow, black: let black, alpha: let alpha):
            return NSColor(deviceCyan: cyan/100, magenta: magenta/100, yellow: yellow/100, black: black/100, alpha: alpha)
        case .hsb(hue: let hue, saturation: let saturation, brightness: let brightness):
            return NSColor(calibratedHue: hue, saturation: saturation, brightness: brightness, alpha: 1)
        case .hsba(hue: let hue, saturation: let saturation, brightness: let brightness, alpha: let alpha):
            return NSColor(calibratedHue: hue, saturation: saturation, brightness: brightness, alpha: alpha)
        case .HSB(hue: let hue, saturation: let saturation, brightness: let brightness):
            return NSColor(calibratedHue: hue/360, saturation: saturation/100, brightness: brightness/100, alpha: 1)
        case .HSBa(hue: let hue, saturation: let saturation, brightness: let brightness, alpha: let alpha):
            return NSColor(calibratedHue: hue/360, saturation: saturation/100, brightness: brightness/100, alpha: alpha)
        }
    }
    
    //MARK: CGColor
    
    var cgColor: CGColor {
        
        switch self {
        case .unknown: return .clear
        case .clear: return .clear
        case .black(alpha: let alpha):
            return CGColor(gray: 0, alpha: alpha)
        case .white(alpha: let alpha):
            return CGColor(gray: 1, alpha: alpha)
        case .gray(gray: let gray, alpha: let alpha):
            return CGColor(gray: gray, alpha: alpha)
        case .rgb(red: let red, green: let green, blue: let blue):
            return CGColor(red: red, green: green, blue: blue, alpha: 1)
        case .RGB(red: let red, green: let green, blue: let blue):
            return CGColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
        case .rgba(red: let red, green: let green, blue: let blue, alpha: let alpha):
            return CGColor(red: red, green: green, blue: blue, alpha: alpha)
        case .RGBa(red: let red, green: let green, blue: let blue, alpha: let alpha):
            return CGColor(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
        case .cmyk(cyan: let cyan, magenta: let magenta, yellow: let yellow, black: let black):
            return CGColor(genericCMYKCyan: cyan, magenta: magenta, yellow: yellow, black: black, alpha: 1)
        case .cmyka(cyan: let cyan, magenta: let magenta, yellow: let yellow, black: let black, alpha: let alpha):
            return CGColor(genericCMYKCyan: cyan, magenta: magenta, yellow: yellow, black: black, alpha: alpha)
        case .CMYK(cyan: let cyan, magenta: let magenta, yellow: let yellow, black: let black):
            return CGColor(genericCMYKCyan: cyan/100, magenta: magenta/100, yellow: yellow/100, black: black/100, alpha: 1)
        case .CMYKA(cyan: let cyan, magenta: let magenta, yellow: let yellow, black: let black, alpha: let alpha):
            return CGColor(genericCMYKCyan: cyan/100, magenta: magenta/100, yellow: yellow/100, black: black/100, alpha: alpha)
        default: return nsColor.cgColor
        }
    }
    
    //MARK: String
    
    static let commas = CharacterSet(charactersIn: " ,(){}[]:;")
    
    struct C {
        var prefix: String?
        var parts: [Colour.Part]
        
        init(_ string: String) {
            let bits = string.components(separatedBy: Colour.commas)
            prefix = bits.first?.trimmingCharacters(in: .whitespaces) ?? ""
            parts = bits.compactMap{ Double($0) }.map{ Colour.Part($0) }
        }
        
        subscript(_ i: Int) -> Part {
            guard parts.indices.contains(i) else { return 0 }
            return parts[i]
        }
        
        var gray: Colour.Part { self[0] }
        var red: Colour.Part { self[0] }
        var green: Colour.Part { self[1] }
        var blue: Colour.Part { self[2] }
        var cyan: Colour.Part { self[0] }
        var magenta: Colour.Part { self[1] }
        var yellow: Colour.Part { self[2] }
        var black: Colour.Part { self[3] }
        var hue: Colour.Part { self[0] }
        var saturation: Colour.Part { self[1] }
        var brightness: Colour.Part { self[2] }
        
        var alpha: Colour.Part { parts.last ?? 1 }
    }
    
    //MARK: String
    
    init(_ string: String) {
        let c = C(string)
        switch c.prefix {
        case "clear" :
            self = .clear
        case "black" :
            self = .black(alpha: c.alpha)
        case "white" :
            self = .white(alpha: c.alpha)
        case "gray" :
            self = .gray(gray: c.gray, alpha: c.alpha)
        case "rgb" :
            self = .rgb(red: c.red, green: c.green, blue: c.blue)
        case "rgba" :
            self = .rgba(red: c.red, green: c.green, blue: c.blue, alpha: c.alpha)
        case "RGBa" :
            self = .RGBa(red: c.red, green: c.green, blue: c.blue, alpha: c.alpha)
        case "cmyk" :
            self = .cmyk(cyan: c.cyan, magenta: c.magenta, yellow: c.yellow, black: c.black)
        case "cmyka" :
            self = .cmyka(cyan: c.cyan, magenta: c.magenta, yellow: c.yellow, black: c.black, alpha: c.alpha)
        case "CMYK" :
            self = .CMYK(cyan: c.cyan, magenta: c.magenta, yellow: c.yellow, black: c.black)
        case "CMYKA" :
            self = .CMYKA(cyan: c.cyan, magenta: c.magenta, yellow: c.yellow, black: c.black, alpha: c.alpha)
        case "hsb" :
            self = .hsb(hue: c.hue, saturation: c.saturation, brightness: c.brightness)
        case "hsba" :
            self = .hsba(hue: c.hue, saturation: c.saturation, brightness: c.brightness, alpha: c.alpha)
        case "HSB" :
            self = .HSB(hue: c.hue, saturation: c.saturation, brightness: c.brightness)
        case "HSBa" :
            self = .hsba(hue: c.hue, saturation: c.saturation, brightness: c.brightness, alpha: c.alpha)
        default: self = .clear
        }
        
    }
    
    var string: String {
        switch self {
        case .unknown: return ""
        case .clear: return "clear"
        case .black(alpha: let alpha):
            return "black \(alpha)"
        case .white(alpha: let alpha):
            return "white \(alpha)"
        case .gray(gray: let gray, alpha: let alpha):
            return "gray \(gray) \(alpha)"
        case .rgb(red: let red, green: let green, blue: let blue):
            return "rgb \(red) \(green) \(blue)"
        case .RGB(red: let red, green: let green, blue: let blue):
            return "RGB \(red) \(green) \(blue)"
        case .rgba(red: let red, green: let green, blue: let blue, alpha: let alpha):
            return "rgba \(red) \(green) \(blue) \(alpha)"
        case .RGBa(red: let red, green: let green, blue: let blue, alpha: let alpha):
            return "RGBa \(red) \(green) \(blue) \(alpha)"
        case .cmyk(cyan: let cyan, magenta: let magenta, yellow: let yellow, black: let black):
            return "cmyk \(cyan) \(magenta) \(yellow) \(black)"
        case .cmyka(cyan: let cyan, magenta: let magenta, yellow: let yellow, black: let black, alpha: let alpha):
            return "cmyka \(cyan) \(magenta) \(yellow) \(black) \(alpha)"
        case .CMYK(cyan: let cyan, magenta: let magenta, yellow: let yellow, black: let black):
            return "CMYK \(cyan) \(magenta) \(yellow) \(black))"
        case .CMYKA(cyan: let cyan, magenta: let magenta, yellow: let yellow, black: let black, alpha: let alpha):
            return "CMYKA \(cyan) \(magenta) \(yellow) \(black) \(alpha)"
        case .hsb(hue: let hue, saturation: let saturation, brightness: let brightness):
            return "hsb \(hue) \(saturation) \(brightness)"
        case .hsba(hue: let hue, saturation: let saturation, brightness: let brightness, alpha: let alpha):
            return "hsba \(hue) \(saturation) \(brightness) \(alpha)"
        case .HSB(hue: let hue, saturation: let saturation, brightness: let brightness):
            return "HSB \(hue) \(saturation) \(brightness))"
        case .HSBa(hue: let hue, saturation: let saturation, brightness: let brightness, alpha: let alpha):
            return "HSBa \(hue) \(saturation) \(brightness) \(alpha)"
        }
    }
    
    var label: String {
        let f = NumberFormatter()
        f.maximumFractionDigits = 3
        let c = C(string)
        var qwerty = (c.prefix ?? "") + " "
        qwerty += c.parts.map{ f.string(from: NSNumber(value: $0)) ?? "\($0)" }.joined(separator: ", ")
        return qwerty
    }
    
}

extension Colour : CustomStringConvertible {
    
    public var description: String {
        string
    }
}

extension Colour {
    //MARK: Static named colours
    
    static let black    = Colour("black")
    static let white    = Colour("white")
    static let grey     = Colour("gray")
    
    static let red      = Colour.rgb(red: 1.0, green: 0.0,  blue: 0.0)
    static let green    = Colour.rgb(red: 0.0, green: 1.0,  blue: 0.0)
    static let blue     = Colour.rgb(red: 0.0, green: 0.0,  blue: 1.0)
    
    static let yellow   = Colour.rgb(red: 1.0, green: 1.0,  blue: 0.0)
    static let cyan     = Colour.rgb(red: 0.0, green: 1.0,  blue: 1.0)
    static let magenta  = Colour.rgb(red: 1.0, green: 0.0,  blue: 1.0)
    
    static let cayenne  = Colour.rgb(red: 0.5, green: 0.0,  blue: 0.0)
    static let clover   = Colour.rgb(red: 0.0, green: 0.5,  blue: 0.0)
    static let navy     = Colour.rgb(red: 0.0, green: 0.0,  blue: 0.5)
    
    static let olive    = Colour.rgb(red: 0.5, green: 0.5,  blue: 0.0)
    static let purple   = Colour.rgb(red: 0.5, green: 0.0,  blue: 0.5)
    static let teal     = Colour.rgb(red: 0.0, green: 0.5,  blue: 0.5)
    
    static let mandarin = Colour.rgb(red: 1.0, green: 0.5,  blue: 0.0)
    static let pink     = Colour.rgb(red: 1.0, green: 0.0,  blue: 0.5)
    static let lime     = Colour.rgb(red: 0.5, green: 1.0,  blue: 0.0)
    static let spring   = Colour.rgb(red: 0.0, green: 1.0,  blue: 0.5)
    static let violet   = Colour.rgb(red: 0.5, green: 0.0,  blue: 1.0)
    static let aqua     = Colour.rgb(red: 0.0, green: 0.5,  blue: 1.0)
    
    static let peach    = Colour.rgb(red: 1.0, green: 0.5,  blue: 0.5)
    static let flora    = Colour.rgb(red: 0.5, green: 1.0,  blue: 0.5)
    static let lavender = Colour.rgb(red: 0.5, green: 0.5,  blue: 1.0)
    
    static let orange   = Colour.rgb(red: 1.0, green: 0.25,  blue: 0.0)
    static let coral    = Colour.rgb(red: 1.0, green: 0.0,  blue: 0.25)
    static let brightgreen   = Colour.rgb(red: 0.25, green: 1.0,  blue: 0.0)
    static let brightindigo   = Colour.rgb(red: 0.25, green: 0.0,  blue: 1.0)
    
    
    static let coffee   = Colour.rgb(red: 0.5, green: 0.25,  blue: 0.0)
    static let maroon   = Colour.rgb(red: 0.5, green: 0.0,  blue: 0.25)
    static let fern     = Colour.rgb(red: 0.25, green: 0.5,  blue: 0.0)
    static let moss     = Colour.rgb(red: 0.0, green: 0.5,  blue: 0.25)
    static let indigo   = Colour.rgb(red: 0.25, green: 0.0, blue: 0.5)
    static let ocean    = Colour.rgb(red: 0.0, green: 0.25,  blue: 0.5)
    
    static let brown    = Colour.rgb(red: 0.6, green: 0.4,  blue: 0.2)
    static let gold     = Colour.rgb(red: 1.0, green: 0.75,  blue: 0.0)
    static let silver   = Colour.rgb(red: 0.75, green: 0.75,  blue: 0.75)
    static let bronze   = Colour.rgb(red: 0.75, green: 0.5,  blue: 0.25)
}
