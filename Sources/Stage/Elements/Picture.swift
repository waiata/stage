//
//  Picture.swift
//  Stage
//
//  Created by Neal Watkins on 2022/2/1.
//

import Cocoa

public struct Picture: Codable {
    
    var url: URL?
    var fit: Fit?
    
    enum CodingKeys: String, CodingKey {
        case url
        case fit
    }
    
    var content: NSImage? {
        guard let url = url else { return nil }
        return NSImage(contentsOf: url)
    }
    
    var nsImage: NSImage {
        return content ?? NSImage()
    }
    
    var coreImage: CIImage {
        guard let url = url else { return CIImage() }
        return CIImage(contentsOf: url) ?? CIImage()
    }
    
    var cgImage: CGImage {
        coreImage.cgImage!
    }
    
    var label: String {
        url?.deletingPathExtension().lastPathComponent ?? ""
    }
    
    enum Fit: String, Codable, CaseIterable {
        case fit
        case fill
        case stretch
        case centre
        case topLeft
        case top
        case topRight
        case right
        case bottomRight
        case bottom
        case bottomLeft
        case left
        
        var gravity: CALayerContentsGravity {
            switch self {
                
            case .fit: return .resizeAspect
            case .fill: return .resizeAspectFill
            case .stretch: return .resize
            case .centre: return .center
            case .top: return .top
            case .topLeft: return .topLeft
            case .topRight: return .topRight
            case .bottom: return .bottom
            case .bottomLeft: return .bottomLeft
            case .bottomRight: return .bottomRight
            case .left: return .left
            case .right: return .right
            }
        }
        
        var label: String {
            self.rawValue.localizedCapitalized
        }
    }
    
}
