//
//  Dimension.swift
//  Stage
//
//  Created by Neal Watkins on 2022/3/21.
//

import Foundation

public struct Dimension: Codable, Equatable {
    
    //MARK: Coding
    
    var size: CGFloat = 0
    var unit: Unit = .pixels
    
    enum CodingKeys: String, CodingKey {
        case size
        case unit
    }
    
    init(_ size: Double = 0, _ unit: Unit = .pixels ) {
        self.size = size
        self.unit = unit
    }
    
    //MARK: Unit
    
    enum Unit: String, Codable, CaseIterable {
        case pixels = "px"
        case portion = "x"
        case percent = "%"
    }
    
    //MARK: Labeling
    
    var label: String {
        return "\(size)" + unit.rawValue
    }
    
    //MARK: Maths

    func pixels(_ within: CGFloat) -> CGFloat {
        switch unit {
        case .pixels    : return size
        case .portion   : return size * within
        case .percent   : return size/100 * within
        }
    }

    mutating func toggle(_ within: CGFloat = 1) {
        guard within != 0 else { return }
        switch unit {
        case .pixels:
            unit = .portion
            size = size / within
        case .portion:
            unit = .pixels
            size = size * within
        case .percent:
            unit = .pixels
            size = size * within * 0.01
        }
    }
    
    mutating func shift(_ px: CGFloat, within: CGFloat = 1) {
        switch unit {
        case .pixels: size += px
        case .portion: size = (size * within + px) / within
        case .percent: size = (size * 0.01 * within + px) / within * 0.01
        }
    }
    
    mutating func scale(_ by: CGFloat) {
        size = size * by
    }
    
    static func *(_ float: CGFloat, _ dimension: Dimension) -> CGFloat {
        return dimension.pixels(float)
    }
    static func /(_ float: CGFloat, _ dimension: Dimension) -> CGFloat {
        return float / dimension.pixels(float)
    }
    static func +(_ float: CGFloat, _ dimension: Dimension) -> CGFloat {
        return float + dimension.pixels(float)
    }
    static func -(_ float: CGFloat, _ dimension: Dimension) -> CGFloat {
        return float - dimension.pixels(float)
    }
    
    //MARK: Constants
    
    static let zero = Dimension(0.0, .portion)
    static let half = Dimension(0.5, .portion)
    static let full = Dimension(1.0, .portion)
    
    
    
//    static func ==(l: Dimension, r: Dimension) -> Bool {
//        return l.size == r.size && l.unit == r.unit
//    }
}
