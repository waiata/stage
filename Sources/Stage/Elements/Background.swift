//
//  Background.swift
//  Stage
//
//  Created by Neal Watkins on 2022/2/1.
//

import Cocoa

public struct Background: Codable {
    
    var colour: Colour?
    var picture: Picture?
    
    func paint(on layer: CALayer) {
        layer.backgroundColor = colour?.cgColor
        layer.contentsGravity = picture?.fit?.gravity ?? .resizeAspect
        layer.contents = picture?.content
    }
    
}
