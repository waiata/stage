//
//  Placement.swift
//  Stage
//
//  Created by Neal Watkins on 2022/3/21.
//

import Foundation

//MARK: Placement

public struct Placement: Codable {
    
    var size = XY.full
    var site = XY.zero
    var edge = Alignment.centreMiddle
    
    func within(_ rect: CGRect) -> CGRect {
        let size = CGSize(
            width: size.x.pixels(rect.width),
            height: size.y.pixels(rect.height)
        )
        let origin = CGPoint(
            x: site.x.pixels(rect.width) + edge.anchorX * (rect.width - size.width) + rect.origin.x,
            y: site.y.pixels(rect.height) + edge.anchorY * (rect.height - size.height) + rect.origin.y
        )
        return CGRect(origin: origin, size: size)
    }
    
    //MARK: XY
    
    struct XY: Codable, Equatable {
        
        var x: Dimension
        var y: Dimension
        
        mutating func shift(x: CGFloat = 0, y: CGFloat = 0) {
            self.x.shift(x)
            self.y.shift(y)
        }
        
        mutating func scale(x: CGFloat = 0, y: CGFloat = 0) {
            self.x.scale(x)
            self.y.scale(y)
        }
        
        //MARK: Edges
        
        static let zero = XY(x: .zero, y: .zero)
        static let full = XY(x: .full, y: .full)
        static let core = XY(x: .half, y: .half)
        static let half = XY(x: .half, y: .half)
        static let leftTop = XY(x: .zero, y: .full)
        static let leftMid = XY(x: .zero, y: .half)
        static let leftBot = XY(x: .zero, y: .zero)
        static let centTop = XY(x: .half, y: .full)
        static let centMid = XY(x: .half, y: .half)
        static let centBot = XY(x: .half, y: .zero)
        static let rightTop = XY(x: .full, y: .full)
        static let rightMid = XY(x: .full, y: .half)
        static let rightBot = XY(x: .full, y: .zero)
        
    }
    
    
}
