//
//  Alignment.swift
//  Stage
//
//  Created by Neal Watkins on 2022/3/24.
//

import Foundation

public struct Alignment: Codable, Equatable {
    
    var horizontal = Horizontal.none
    var vertical = Vertical.none
    
    enum Horizontal: String, Codable {
        case none
        case left
        case centre
        case right
    }
    
    enum Vertical: String, Codable {
        case none
        case top
        case middle
        case bottom
    }
    
    init(_ horizontal: Horizontal = .none) {
        self.horizontal = horizontal
    }
    
    init(_ vertical: Vertical = .none) {
        self.vertical = vertical
    }
    
    init(_ horizontal: Horizontal = .none, _ vertical: Vertical = .none) {
        self.horizontal = horizontal
        self.vertical = vertical
    }
    
    init(_ edge: Alignment = .centreMiddle, offset: CGPoint = .zero) {
        self.horizontal = edge.horizontal
        self.vertical = edge.vertical
    }
    
    
    var anchorX: CGFloat {
        switch horizontal {
            
        case .none: return 0.5
        case .left: return 0.0
        case .centre: return 0.5
        case .right: return 1.0
        }
    }
    
    var anchorY: CGFloat {
        switch vertical {
            
        case .none: return 0.5
        case .top: return 0.0
        case .middle: return 0.5
        case .bottom: return 1.0
        }
    }
    
    var anchorPoint: CGPoint {
        return CGPoint(x: anchorX, y: anchorY)
    }
    
    public static let none = Alignment(.none, .none)
    public static let left = Alignment(.left)
    public static let centre = Alignment(.centre)
    public static let right = Alignment(.right)
    public static let top = Alignment(.top)
    public static let middle = Alignment(.middle)
    public static let bottom = Alignment(.bottom)
    public static let leftTop = Alignment(.left, .top)
    public static let leftMiddle = Alignment(.left, .middle)
    public static let leftBottom = Alignment(.left, .bottom)
    public static let centreTop = Alignment(.centre, .top)
    public static let centreMiddle = Alignment(.centre, .middle)
    public static let centreBottom = Alignment(.centre, .bottom)
    public static let rightTop = Alignment(.right, .top)
    public static let rightMiddle = Alignment(.right, .middle)
    public static let rightBottom = Alignment(.right, .bottom)
    
}
