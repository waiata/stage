//
//  PlaceField.swift
//  Stage
//
//  Created by Neal Watkins on 2022/3/21.
//

import Cocoa

@IBDesignable
public class DimensionField: NumberField {
      
    var fullSize = 1920.0 {
        didSet {
            if fullSize == 0 { fullSize = oldValue }
        }
    }
    
    var dimension = Dimension(0) {
        didSet {
            number = dimension.size
        }
    }
    
    override func bump(_ by: Double) {
        var bump = by
        switch dimension.unit {
        case .pixels:
            bump = by
        case .portion:
            bump = by / fullSize
        case .percent:
            bump = by / fullSize * 100
        }
        dimension.size += bump
    }
    
    func unit(from string: String) -> Dimension.Unit? {
        return Dimension.Unit.allCases.first {
            string.hasSuffix($0.rawValue)
        }
    }
    
    override func read() {
        let string = stringValue.trimmingCharacters(in: .whitespaces)
        let unit = unit(from: string) ?? dimension.unit
        number = number(from: string)
        dimension = Dimension(number, unit)
    }
    
    override func render() {
        format.maximumFractionDigits = decimals
        let n = format.string(from: NSNumber(value: number)) ?? "\(number)"
        stringValue = n + dimension.unit.rawValue
    }
}
