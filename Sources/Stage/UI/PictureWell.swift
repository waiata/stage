//
//  ImageWell.swift
//  Lucky
//
//  Created by Neal on 6-18-17.
//  Copyright © 2017 waiata. All rights reserved.
//

import Cocoa

@IBDesignable
public class PictureWell: NSControl {
    
    @IBInspectable var backgroundColor: NSColor = .controlBackgroundColor
    @IBInspectable var borderColor: NSColor = .controlColor
    @IBInspectable var borderWidth: CGFloat = 1
    @IBInspectable var cornerRadius: CGFloat = 1
    
    
    
    var picture = Picture() {
        didSet {
            render()
        }
    }
    
    //MARK: Life
    
    public override func awakeFromNib() {
        build()
    }
    
    //MARK: IB
    
    public override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        build()
    }
    
    
    //MARK: Render
    func build() {
        registerDrop()
        wantsLayer = true
        layer?.backgroundColor = backgroundColor.cgColor
        layer?.borderColor = borderColor.cgColor
        layer?.borderWidth = borderWidth
        layer?.cornerRadius = cornerRadius
    }
    
    func render() {
        
        if let url = picture.url  {
            layer?.contents = NSImage(byReferencing: url)
            layer?.contentsGravity = picture.fit?.gravity ?? .center
        } else { // no image url selected
            layer?.contents = nil
        }
    }
    
    //MARK: Go
    
    func go() {
        self.sendAction(action, to: target)
    }
    
    //MARK: Interaction
    
    public override func mouseDown(with event: NSEvent) {
        focus()
        chooseFile()
    }
    
    public override func rightMouseDown(with event: NSEvent) {
        guard let windowView = window?.contentView else { return }
        focus()
        let location = self.convert(event.locationInWindow, from: windowView)
        menu().popUp(positioning: nil, at: location, in: self)
        
    }
    
    func chooseFile() {
        let panel = NSOpenPanel()
        panel.allowsMultipleSelection = false
        panel.canChooseDirectories = false
//        panel.allowedFileTypes = NSImage.imageTypes
        let ok = panel.runModal()
        guard ok == .OK else { return }
        picture.url = panel.url
        go()
    }
    
    
    func focus() {
        window?.makeFirstResponder(self)
        layer?.borderWidth = borderWidth * 4
        layer?.borderColor = NSColor.keyboardFocusIndicatorColor.cgColor
    }
    
    public override func resignFirstResponder() -> Bool {
        layer?.borderWidth = borderWidth
        layer?.borderColor = borderColor.cgColor
        NSColorPanel.shared.close()
        return true
    }
    
    //MARK: Contextual Menu
    
    func menu() -> NSMenu {
        let menu = NSMenu()
        for fit in Picture.Fit.allCases {
            let item = NSMenuItem(
                title: fit.label,
                action: #selector(changeFit(_:)),
                keyEquivalent: ""
            )
            item.target = self
            item.representedObject = fit
            item.state = picture.fit == fit ? .on : .off
            menu.addItem(item)
        }
        let kill = NSMenuItem(
            title: "Clear Image".localizedCapitalized,
            action: #selector(clearImage(_:)),
            keyEquivalent: ""
        )
        menu.addItem(.separator())
        kill.target = self
        kill.isEnabled = picture.url != nil
        menu.addItem(kill)
        menu.autoenablesItems = true
        return menu
    }
    
    @objc func clearImage(_ sender: NSMenuItem) {
        picture.url = nil
        go()
    }
    
    @objc func changeFit(_ sender: NSMenuItem) {
        guard let fit = sender.representedObject as? Picture.Fit else { return }
        picture.fit = fit
        go()
    }
    
    //MARK: Drag and Drop
    
    func registerDrop() {
        let types = [NSPasteboard.PasteboardType(kUTTypeURL as String)]
        registerForDraggedTypes(types)
    }
    
    public override func draggingEntered(_ sender: NSDraggingInfo) -> NSDragOperation  {
        return NSDragOperation.copy
    }
    
    public override func prepareForDragOperation(_ sender: NSDraggingInfo) -> Bool {
        return true
    }
    
    public override func performDragOperation(_ sender: NSDraggingInfo) -> Bool {
        let pb: NSPasteboard = sender.draggingPasteboard
        picture.url = NSURL(from: pb) as URL?
        go()
        return super.performDragOperation(sender)
    }
}
