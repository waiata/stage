//
//  NumberField.swift
//  One Love
//
//  Created by Neal Watkins on 2021/10/4.
//  Copyright © 2021 Waiata. All rights reserved.
//

import Cocoa

@IBDesignable
public class NumberField: NSTextField {
    
    @IBInspectable var bumpStep : Double = 1
    @IBInspectable var bigBumpStep : Double = 10
    @IBInspectable var modulate : Double = 0.0
    @IBInspectable var decimals : Int = -1
    
    enum Maths {
        case add
        case subtract
        case divide
        case multiply
        case power
        
        var delimiters: CharacterSet {
            switch self {
            case .add: return ["+"]
            case .subtract: return ["-"]
            case .multiply: return ["*"]
            case .divide: return ["/", "÷"]
            case .power: return ["^"]
            }
        }
        
        var next: Maths? {
            switch self {
            case .add: return .subtract
            case .subtract: return .multiply
            case .multiply: return .divide
            case .divide: return .power
            case .power: return nil
            }
        }
        var start: Double {
            switch self {
            case .add: return 0
            case .subtract: return 0
            case .multiply: return 1
            case .divide: return 1
            case .power: return 1
            }
        }
        
        static func calc(_ string: String) -> Double {
            return Maths.add.calc(string)
        }
        
        func calc(_ string: String) -> Double {
            let nouns = string.components(separatedBy: delimiters)
            switch nouns.count {
            case 0 : return 0
            case 1 : return next?.calc(string) ?? double(string.filter{ "1234567890.".contains($0) })
            default :
                var numbers = nouns.compactMap{ next?.calc($0) ?? double($0) }
                let start = numbers.removeFirst()
                return numbers.reduce(start){ evaluate($0, $1) }
            }
        }
        
        func double(_ string: String) -> Double {
            Maths.formatter.number(from: string)?.doubleValue ?? 0
        }
        
        func evaluate(_ left: Double, _ right: Double) -> Double {
            switch self {
            case .add: return left + right
            case .subtract: return left - right
            case .divide: return left / right
            case .multiply: return left * right
            case .power: return pow(left, right)
            }
        }
        
        static let formatter = NumberFormatter()
    }
    
    public override func awakeFromNib() {
        delegate = self
        read()
    }
    
    public override func textDidEndEditing(_ notification: Notification) {
        read()
        super.textDidEndEditing(notification)
    }
    
    var number: Double = 0 {
        didSet {
            render()
        }
    }
    
    func number(from string: String) -> Double {
        let operators = "-+*÷/^"
        if let trail = string.last,
           operators.contains(trail) {
            return Maths.calc(string + "\(number)")
        } else {
            return Maths.calc(string)
        }
    }
    
    func bump(_ by: Double) {
        number += by
    }
    
    func read() {
        number = number(from: stringValue.trimmingCharacters(in: .whitespaces))
    }
    
    func render() {
        format.maximumFractionDigits = decimals
        stringValue = format.string(from: NSNumber(value: number)) ?? "?"
    }
    
    let format = NumberFormatter()
    
    
}

extension NumberField: NSTextFieldDelegate {
    //MARK: Bump Keys
    
    public func control(_ control: NSControl, textView: NSTextView, doCommandBy commandSelector: Selector) -> Bool {
        switch commandSelector {
        case #selector(moveUp(_:)) : bump(bumpStep)
        case #selector(moveDown(_:)) : bump(-bumpStep)
        case #selector(moveBackward(_:)) : bump(bigBumpStep)
        case #selector(moveForward(_:)) : bump(-bigBumpStep)
        default: return false
        }
        sendAction(action, to: target)
        return true
    }
    
}
