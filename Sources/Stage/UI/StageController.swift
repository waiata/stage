//
//  StageController.swift
//  Stage
//
//  Created by Neal Watkins on 2022/1/29.
//

import Cocoa

public class StageController: NSViewController {

    var stage = Default.stage
    
    @IBOutlet weak var onSwitch: NSSwitch!
    @IBOutlet weak var screenPopUp: ScreenPopUp!
    @IBOutlet weak var stageChooser: StageChooser!
    @IBOutlet weak var siteXField: DimensionField!
    @IBOutlet weak var siteYField: DimensionField!
    @IBOutlet weak var sizeXField: DimensionField!
    @IBOutlet weak var sizeYField: DimensionField!
    @IBOutlet weak var edgeChooser: EdgeChooser!
    @IBOutlet weak var bgColorWell: ColourWell!
    @IBOutlet weak var bgPictureWell: PictureWell!
    
    @IBAction func buildStage(_ sender: Any) {
        stage.build()
    }
    
    @IBAction func renderStage(_ sender: Any) {
        stage.render()
    }
    
    @IBAction func clearBackground(_ sender: Any) {
        stage.background.colour = nil
        render()
    }
    
    @IBAction func colorBackground(_ sender: ColourWell) {
        stage.background.colour = sender.colour
        render()
    }
    
    @IBAction func pictureBackground(_ sender: PictureWell) {
        stage.background.picture = sender.picture
        render()
    }
    
    @IBAction func popSceen(_ sender: ScreenPopUp) {
        stage.screen = sender.screen
        render()
    }
    
    @IBAction func placeSiteX(_ sender: DimensionField) {
        stage.placement.site.x = sender.dimension
        render()
    }
    
    @IBAction func placeSiteY(_ sender: DimensionField) {
        stage.placement.site.y = sender.dimension
        render()
    }
    
    @IBAction func placeSizeX(_ sender: DimensionField) {
        stage.placement.size.x = sender.dimension
        render()
    }
    
    @IBAction func placeSizeY(_ sender: DimensionField) {
        stage.placement.size.y = sender.dimension
        render()
    }
    
    @IBAction func toggleSite(_ sender: Any) {
        stage.placement.site.x.toggle(stage.screen?.bounds.width ?? stage.size.width)
        stage.placement.site.y.toggle(stage.screen?.bounds.height ?? stage.size.height)
        render()
    }
    
    @IBAction func toggleSize(_ sender: Any) {
        stage.placement.size.x.toggle(stage.screen?.bounds.width ?? stage.size.width)
        stage.placement.size.y.toggle(stage.screen?.bounds.height ?? stage.size.height)
        render()
    }
    
    @IBAction func toggleCursor(_ sender: NSButton) {
        stage.on.toggle()
        render()
    }
    
    @IBAction func chooseEdge(_ sender: EdgeChooser) {
        stage.placement.edge = sender.edge
        render()
    }
    
    
    @objc func render() {
        
        onSwitch.state = stage.on ? .on : .off
        
        screenPopUp.screen = stage.screen
        stageChooser.stage = stage
        stageChooser.render()
        
        siteXField.dimension = stage.placement.site.x
        siteYField.dimension = stage.placement.site.y
        sizeXField.dimension = stage.placement.size.x
        sizeYField.dimension = stage.placement.size.y
        if let size = stage.screen?.bounds.size {
            siteXField.fullSize = size.width
            siteYField.fullSize = size.height
            sizeXField.fullSize = size.width
            sizeYField.fullSize = size.height
        }
        
        edgeChooser.edge = stage.placement.edge
        
        bgColorWell.colour = stage.background.colour ?? .clear
        bgPictureWell.picture = stage.background.picture ?? Picture()
        Default.stage = stage
    }
    
    
    //MARK: Life
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        stage.build()
        render()
        observe()
    }
    
    func observe() {
        NotificationCenter.default.addObserver(self, selector: #selector(render), name: StageChooser.Notification.selectScreen, object: stageChooser)
    }
    
    struct Default {
        
        static var stage: Stage {
            get {
                guard let data = UserDefaults.standard.data(forKey: "Stage"),
                      let stage = try? JSONDecoder().decode(Stage.self, from: data)
                else { return Stage() }
                return stage
            }
            set {
                guard let data = try? JSONEncoder().encode(newValue)
                else { return }
                UserDefaults.standard.set(data, forKey: "Stage")
            }
        }
    }

}



