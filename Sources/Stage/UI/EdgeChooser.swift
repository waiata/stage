//
//  EdgeChooser.swift
//  Stage
//
//  Created by Neal Watkins on 2022/3/24.
//

import Cocoa

@IBDesignable
public class EdgeChooser: NSControl {
    
    //MARK: Design
    
    @IBInspectable var buttonColor: NSColor = .controlColor
    @IBInspectable var selectedColor: NSColor = .controlAccentColor
    
    
    //MARK: Connections
    
    var buttons: [EdgeButton] {
        subviews.filter{ $0 is EdgeButton } as! [EdgeButton]
    }
    
    var boundsMin: CGFloat {
        min(frame.width, frame.height)
    }
    
    var buttonSquare: CGFloat {
        boundsMin / 3
    }
    
    var edge: Alignment = .none {
        didSet { render() }
    }
    
    //MARK: Render
    
    func build() {
        // remove any old subviews
        self.subviews = []
        // add a subview for each edge
        button(.leftTop)
        button(.centreTop)
        button(.rightTop)
        button(.leftMiddle)
        button(.centreMiddle)
        button(.rightMiddle)
        button(.leftBottom)
        button(.centreBottom)
        button(.rightBottom)
        render()
    }
    
    func button(_ edge: Alignment) {
        let butt = EdgeButton()
        self.addSubview(butt)
        butt.edge = edge
        butt.target = self
        butt.action = #selector(chooseEdge(_:))
    }
    
    @objc func chooseEdge(_ sender: EdgeButton) {
        self.edge = sender.edge
        sendAction(action, to: target)
        render()
    }
    
    
    func render() {
        for button in buttons {
            button.render()
        }
    }
    
    
    //MARK: Life
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        build()
    }
    
    
    //MARK: IB
    
    public override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        build()
    }
    
    //MARK: Geometry
    
   
    
    
    //MARK: XYButton
    
    class EdgeButton: NSButton {
        
        //MARK: Connections
        
        var edge: Alignment = .none {
            didSet {
                render()
            }
        }
        
        var chooser: EdgeChooser? {
            superview as? EdgeChooser
        }
        
        var isSelected: Bool {
            chooser?.edge == self.alignment
        }
        
        //MARK: Render
        
        func render() {
            paint()
            place()
        }
        
        //MARK: Geometry
        
        func place() {
            
            guard let chooser = chooser else { return }
            let square = chooser.buttonSquare
            let working = CGSize(width: chooser.bounds.width - square, height: chooser.bounds.height - square)
            frame.size.width = chooser.buttonSquare
            frame.size.height = chooser.buttonSquare
            frame.origin.x = working.width * edge.anchorX
            frame.origin.y = working.height * edge.anchorY
        }
        
        func paint() {
            title = ""
            image = nil
            bezelStyle = .regularSquare
            setButtonType(.onOff)
            state = isSelected ? .on : .off
        }
        
        
    }
    
}
