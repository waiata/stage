//
//  StageChooser.swift
//
//  Created by Neal Watkins on 2022/3/17.
//

import Cocoa

@IBDesignable
public class StageChooser: NSView {
    
    //MARK: Design
    
    @IBInspectable var borderColor: NSColor = .controlShadowColor
    @IBInspectable var hoverColor: NSColor = .controlHighlightColor
    @IBInspectable var selectedColor: NSColor = .controlAccentColor
    @IBInspectable var overlayOpacity: Float = 0.69
    
    
    //MARK: Connections
    
    
    var stage: Stage? {
        didSet {
            render()
        }
    }
    
    var buttons: [ScreenButton] {
        return self.subviews.filter{ $0 is ScreenButton } as! [ScreenButton]
    }
    
    func button(for screen: NSScreen) -> ScreenButton? {
        buttons.first{ $0.screen == screen }
    }
    
    var overlay = CALayer()
    
    //MARK: Render
    
    @objc func build() {
        // remove any old subviews
        self.subviews = []
        // add a subview for each screen
        for screen in NSScreen.screens {
            add(screen: screen)
        }
        render()
    }
    
    func add(screen: NSScreen) {
        let butt = ScreenButton()
        self.addSubview(butt)
        butt.target = self
        butt.action = #selector(chooseScreen(_:))
        butt.screen = screen
    }
    
    @objc func chooseScreen(_ sender: ScreenButton) {
        stage?.screen = sender.screen
        render()
        
        //send notification
        NotificationCenter.default.post(name: Notification.selectScreen, object: self)
    }
    
    
    func render() {
        // unhighlight all screens except selected
        for button in buttons {
            button.selected = (button.screen == stage?.screen)
        }
        
        renderOverlay()
    }
    
    func renderOverlay() {
        guard let stage = stage else { return }

        let scaled = Geometry(bounds)
        overlay.frame = scaled.frame(for: stage.frame)
        
        
        stage.background.paint(on: overlay)
        
        overlay.opacity = overlayOpacity
        
    }
    
    //MARK: Life
    
    
    public override func draw(_ dirtyRect: NSRect) {
        build()
    }
    
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        self.wantsLayer = true
        layer?.addSublayer(overlay)
        overlay.zPosition = 999
        overlay.masksToBounds = true
        observe()
    }
    
    /// Sets observer for change in Display settings
    func observe() {
        NotificationCenter.default.addObserver(self, selector: #selector(build), name: NSApplication.didChangeScreenParametersNotification, object: NSApplication.shared)
    }
    
    //MARK: Notifications
    
    struct Notification {
        static let selectScreen = NSNotification.Name("StageChooser.SelectScreen")
    }
    
    //MARK: IB
    
    public override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        build()
    }
    
    //MARK: Geometry
    
    struct Geometry {
        
        var frame: CGRect = .zero
        var ratio: CGFloat = 0
        
        init(_ extents: CGRect) {
            frame = NSScreen.extentsOfAllScreens
            guard !frame.isEmpty else { return }
            
            ratio = min(extents.width / frame.width, extents.height / frame.height)
            
            frame.size.width *= ratio
            frame.size.height *= ratio
            
            frame.origin.x = (extents.width - frame.width) / 2 - frame.origin.x * ratio
            frame.origin.y = (extents.height - frame.height) / 2 - frame.origin.y * ratio
        }
        
        func frame(for rect: CGRect) -> CGRect {
            CGRect(
                x: rect.origin.x * ratio + frame.minX,
                y: rect.origin.y * ratio + frame.minY,
                width: rect.size.width * ratio,
                height: rect.size.height * ratio
            )
        }
    }
    
    
    //MARK: ScreenButton
    
    class ScreenButton: NSButton {
        
        //MARK: Connections
        
        var screen: NSScreen? {
            didSet {
                render()
            }
        }
        
        var selected: Bool = false {
            didSet {
                highlight()
            }
        }
        
        
        //MARK: Render
        
        func render() {
            place()
            paint()
            label()
        }
        
        //MARK: Geometry
        
        func place() {
            
            guard let screen = self.screen,
                  let bounds = superview?.bounds
            else { return }
            
            let scaled = Geometry(bounds)
            frame = scaled.frame(for: screen.frame)
            track()
        }
        
        //MARK: Tracking
        
        var trackingTag: Int?
        
        func track() {
            if let tag = trackingTag {
                removeTrackingRect(tag)
            }
            trackingTag = addTrackingRect(self.bounds, owner: self, userData: nil, assumeInside: false)
        }
        
        
        func paint() {
            self.wantsLayer = true
            let wall = CALayer()
            wall.frame = self.bounds
            wall.backgroundColor = screen?.backgroundColor ?? Default.background
            wall.contents = screen?.wallpaper
            wall.zPosition = -1000
            layer?.addSublayer(wall)
            menubar()
        }
        
        func menubar() {
            guard let screen = self.screen,
                  screen.isMain
            else { return }
            let bar = CALayer()
            bar.frame.size.width = self.frame.width
            bar.frame.size.height = Default.menubarHeight
            bar.frame.origin.y = 0
            bar.backgroundColor = Default.menubarColor
            self.layer?.addSublayer(bar)
        }
        
        func label() {
            guard let screen = self.screen else { return }
            let tit = screen.name + "\n" + screen.resolutionLabel
            title = tit
            toolTip = tit
        }
        
        
        //MARK: Highlight
        
        /// put border around layer
        /// - Parameter glow: how to style the border - defaults to .non, will be overridden as .selected if selected is true
        func highlight(_ highlight: Highlight = .non) {
            let glow: Highlight = selected ? .selected : highlight
            self.layer?.borderWidth = glow.width
            self.layer?.borderColor = glow.border
            self.layer?.opacity = glow.opacity
        }
        
        enum Highlight {
            case non
            case hover
            case selected
            
            var border: CGColor {
                switch self {
                case .non : return .clear
                case .hover : return .black
                case .selected : return .white
                }
            }
            
            var opacity: Float {
                switch self {
                case .non : return 0.5
                case .hover : return 1.0
                case .selected : return 1.0
                }
            }
            
            var width: CGFloat {
                return 2.0
            }
        }
        
        
        
        public override func mouseEntered(with event: NSEvent) {
            self.highlight(.hover)
        }
        
        public override func mouseExited(with event: NSEvent) {
            self.highlight(.non)
        }
        
        
        //MARK: Defaults
        
        struct Default {
            static let background: CGColor = .black
            
            static let menubarColor: CGColor = .white
            static let menubarHeight: CGFloat = 12.0
            static let labelY: CGFloat = 12.0
            static let selectedOpacity: CGFloat = 1
            static let unselectedOpacity: CGFloat = 0.5
        }
        
        
    }
    
}
