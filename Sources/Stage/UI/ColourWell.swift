//
//  ColourWell.swift
//
//  Created by Neal on 12-10-16.
//  Copyright © 2016 waiata. All rights reserved.
//

import Cocoa

@IBDesignable
public class ColourWell: NSControl {
    
    @IBInspectable var backgroundColor: NSColor = .controlBackgroundColor
    @IBInspectable var borderColor: NSColor = .controlColor
    @IBInspectable var borderWidth: CGFloat = 1
    @IBInspectable var cornerRadius: CGFloat = 1
    @IBInspectable var memoryBank: String = "Colour Bank"
    @IBInspectable var memoryCount: Int = 12
    @IBInspectable var showsAlpha : Bool = true
    
    var memory: Memory = Memory()
    
    var colour = Colour() {
        didSet {
            render()
        }
    }
    
    //MARK: Life
    
    public override func awakeFromNib() {
        build()
    }
    
    //MARK: IB
    
    public override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        build()
    }
    
    
    //MARK: Render
    
    
    func build() {
        registerDrop()
        memory.bank = memoryBank
        if memoryCount > 0 { memory.limit = memoryCount }
        wantsLayer = true
        layer?.backgroundColor = backgroundColor.cgColor
        layer?.borderColor = borderColor.cgColor
        layer?.borderWidth = borderWidth
        layer?.cornerRadius = cornerRadius
    }
    
    func render() {
        layer?.backgroundColor = colour.cgColor
    }
    
    //MARK: Go
    
    func go() {
        self.sendAction(action, to: target)
    }
    
    //MARK: Interaction
    
    public override func mouseDown(with event: NSEvent) {
        focus()
        chooseColour()
    }
    
    public override func rightMouseDown(with event: NSEvent) {
        guard let windowView = window?.contentView else { return }
        focus()
        let location = self.convert(event.locationInWindow, from: windowView)
        menu().popUp(positioning: nil, at: location, in: self)
    }
    
    func chooseColour() {
        NSColorPanel.shared.showsAlpha = self.showsAlpha
        NSColorPanel.shared.isContinuous = false
        NSColorPanel.shared.setTarget(self)
        NSColorPanel.shared.setAction(#selector(colorPanelChose(_:)))
        NSColorPanel.shared.color = colour.nsColor
        NSColorPanel.shared.orderFront(self)
        
    }
    
    @objc func colorPanelChose(_ sender: NSColorPanel) {
        self.colour = Colour(sender.color)
        memory.add(self.colour)
        go()
    }
    
    func focus() {
        window?.makeFirstResponder(self)
        layer?.borderWidth = borderWidth * 4
        layer?.borderColor = NSColor.keyboardFocusIndicatorColor.cgColor
    }
    
    public override func resignFirstResponder() -> Bool {
        layer?.borderWidth = borderWidth
        layer?.borderColor = borderColor.cgColor
        NSColorPanel.shared.close()
        return true
    }
    
    
    //MARK: Contextual Menu
    
    func menu() -> NSMenu {
        let menu = NSMenu()
        for colour in memory.list {
            let item = NSMenuItem()
            item.target = self
            item.action = #selector(changeColour(_:))
            item.representedObject = colour
            item.state = colour == self.colour ? .on : .off
            item.title = colour.label
            item.image = swatch(for: colour.nsColor)
            menu.addItem(item)
        }
        
        menu.addItem(.separator())
        
        let clear = NSMenuItem()
        clear.title = "Clear".localizedCapitalized
        clear.action = #selector(clear(_:))
        clear.target = self
        menu.addItem(clear)
        
        menu.addItem(.separator())
        
        let wipe = NSMenuItem()
        wipe.title = "Wipe Memory".localizedCapitalized
        wipe.action = #selector(wipeMemory(_:))
        wipe.target = self
        menu.addItem(wipe)
        
        menu.autoenablesItems = true
        return menu
    }
    
    func swatch(for colour: NSColor, size: CGSize = CGSize(width: 16, height: 16)) -> NSImage? {
        let image = NSImage(size: size)
        image.lockFocus()
        colour.set()
        let rect = NSRect(origin: .zero, size: size)
        rect.fill()
        image.unlockFocus()
        return image
    }
    
    @objc func clear(_ sender: NSMenuItem) {
        colour = .clear
        render()
        go()
    }
    @objc func wipeMemory(_ sender: NSMenuItem) {
        memory.wipe()
        render()
        go()
    }
    
    @objc func changeColour(_ sender: NSMenuItem) {
        guard let colour = sender.representedObject as? Colour else { return }
        self.colour = colour
        
        memory.add(colour)
        go()
    }
    
    //MARK: Drag and Drop
    
    func registerDrop() {
        let types = [NSPasteboard.PasteboardType.color]
        registerForDraggedTypes(types)
    }
    
    public override func draggingEntered(_ sender: NSDraggingInfo) -> NSDragOperation  {
        return NSDragOperation.copy
    }
    
    public override func prepareForDragOperation(_ sender: NSDraggingInfo) -> Bool {
        return true
    }
    
    public override func performDragOperation(_ sender: NSDraggingInfo) -> Bool {
        let pb: NSPasteboard = sender.draggingPasteboard
        if let ns = NSColor(from: pb) {
            self.colour = Colour(ns)
        }
        go()
        return super.performDragOperation(sender)
    }
    
    
    //MARK: Memory
    
    struct Memory {
        var bank: String = "Colour Bank"
        var limit: Int = .max
        
        var store: [String] {
            get {
                UserDefaults.standard.array(forKey: bank) as? [String] ?? []
            }
            set {
                let n = min(newValue.count, limit)
                let save = Array(newValue[..<n])
                UserDefaults.standard.set(save, forKey: bank)
            }
        }
        
        var list: [Colour] {
            store.compactMap{ Colour($0) }
        }
        
        mutating func add(_ colour: Colour) {
            let string = colour.string
            if store.contains(string) { return }
            store.insert(string, at: 0)
        }
        
        mutating func wipe() {
            store = []
        }
    }
}
