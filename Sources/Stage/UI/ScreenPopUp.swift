//
//  ScreenPopUp.swift
//
//  Created by Neal Watkins on 2022/3/30.
//

import Cocoa

public class ScreenPopUp: NSPopUpButton {
    
    var screen: NSScreen? {
        get { selectedItem?.representedObject as? NSScreen }
        set { select(newValue) }
    }
    
    
    //MARK: Render
    
    @objc func populate() {
        removeAllItems()
        for screen in NSScreen.screens {
            menu?.addItem(item(for: screen))
        }
    }
    
    func item(for screen: NSScreen) -> NSMenuItem {
        let item = NSMenuItem()
        item.title = "\(screen.indexLabel). \(screen.name) (\(screen.resolutionLabel))"
        item.representedObject = screen
        return item
    }
    
    func select(_ screen: NSScreen?) {
        let selected = menu?.items.first{ $0.representedObject as? NSScreen == screen }
        select(selected)
    }
    
    
    //MARK: Life
    
    func build() {
        observe()
        populate()
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        build()
    }
    
    /// Sets observer for change in Display settings
    func observe() {
        NotificationCenter.default.addObserver(self, selector: #selector(populate), name: NSApplication.didChangeScreenParametersNotification, object: NSApplication.shared)
    }
    
    //MARK: IB
    
    public override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        build()
    }
    
    
}
