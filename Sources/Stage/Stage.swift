//
//  Stage.swift
//  Stage
//
//  Created by Neal Watkins on 2022/1/29.
//

import Cocoa

public class Stage: Codable {
    
    //MARK: Coding
    
    var on: Bool = true {
        didSet {
            toggle()
        }
    }
    
    var displayID: CGDirectDisplayID? = Default.displayID {
        didSet { place() }
    }
    
    var screenIndex: Int? = nil
    
    var placement = Placement() {
        didSet { place() }
    }
    
    var background = Background() {
        didSet { paint() }
    }
    
    enum CodingKeys: String, CodingKey {
        case on
        case displayID
        case screenIndex
        case placement
        case background
    }
    
    //MARK: Connections
    
    var windowController: NSWindowController?
    
    var window: NSWindow?
    
    var view: NSView?
    
    var layer: CALayer?
    
    //MARK: Geometry
    
    var screen: NSScreen? {
        get {
            if let screen = NSScreen.screen(id: displayID) {
                return screen
            } else if
                let index = screenIndex,
                NSScreen.screens.indices.contains(index) {
                return NSScreen.screens[index]
            } else {
                return NSScreen.screens.last ?? NSScreen.main
            }
        }
        set {
            screenIndex = newValue?.index
            displayID = newValue?.id
        }
    }
    
    /// return frame within screen
    var frame: CGRect {
        guard let bounds = screen?.frame else { return .zero }
        return placement.within(bounds)
    }
    
    var bounds: CGRect {
        return view?.bounds ?? .zero
    }
    
    var size: CGSize {
        return bounds.size
    }
    
    
    //MARK: Build
    
    func build() {
        
        windowController?.close()
        
        window = NSWindow(contentRect: frame,
                          styleMask: .borderless,
                          backing: .buffered,
                          defer: false)
        
        window?.isOpaque = false
        window?.hasShadow = false
        window?.backgroundColor = .clear
        window?.acceptsMouseMovedEvents = true
        
        windowController = NSWindowController(window: window)
        
        view = window?.contentView
        view?.wantsLayer = true
        
        layer = view?.layer
        
        observe()
        
        render()
        
        windowController?.showWindow(self)
    }
    
    /// Sets observer for change in Display settings
    func observe() {
        NotificationCenter.default.addObserver(self, selector: #selector(render), name: NSApplication.didChangeScreenParametersNotification, object: NSApplication.shared)
    }
    
    //MARK: Render
    
    /// redraw
    @objc func render() {
        place()
        paint()
    }
    
    /// position on screen
    func place() {
        windowController?.window?.setFrame(frame, display: true)
        Notification.move(self)
        toggle(true)
        stopCursor()
    }
    
    /// draw background
    func paint() {
        guard let layer = layer else { return }
        background.paint(on: layer)
    }
    
    /// Turn window on or off
    /// - Parameter on: true = show window, false = hide window, nil = use current setting
    func toggle(_ on: Bool? = nil) {
        let onoff = on ?? self.on
        onoff ? windowController?.showWindow(self) : windowController?.window?.orderOut(self)
        Notification.toggle(self)
    }
    
    //MARK: Cursor
    
    /// setup cursor tracking rect
    func stopCursor() {
        guard let view = view else { return }
        view.resetCursorRects()  // cursor hiding
        let cursorSize = NSCursor.current.image.size
        #warning("doesn't allow for large scaled cursor")
        let cursorRect = NSRect(x: 0-cursorSize.width, y: 0, width: view.frame.width + cursorSize.width, height: view.frame.height + cursorSize.height)
        view.addTrackingRect(cursorRect, owner: self, userData: nil, assumeInside: false)
    }
    
    @objc func mouseEntered(_ event: NSEvent) {
        if !Default.allowCursor {
           NSCursor.hide()
        }
    }
    
    @objc func mouseExited(_ event: NSEvent) {
        NSCursor.unhide()
    }
    
    deinit {
        kill()
    }
    
    /// close window
    func kill() {
        windowController?.window?.orderOut(self)
    }
    
    //MARK: Preview
    
    func previewLayer() -> CALayer {
        
        let lay = CALayer()
        lay.frame.size = self.size
        
        return lay
    }
    
    //MARK: Notifications
    
    
    struct Notification {
        
        static let move = NSNotification.Name("Stage Move")
        
        static func move(_ stage: Stage) {
            NotificationCenter.default.post(name: move, object: stage)
        }
        
        static let toggle = NSNotification.Name("Stage Toggle")
        
        static func toggle(_ stage: Stage) {
            NotificationCenter.default.post(name: toggle, object: stage)
        }
    }
    
    //MARK: Defaults
    
    struct Default {
        static var displayID: CGDirectDisplayID? {
            UInt32(UserDefaults.standard.integer(forKey: "Stage Screen ID"))
        }
        
        static var allowCursor: Bool {
            UserDefaults.standard.bool(forKey: "Stage Allow Cursor")
        }
    }
}
